/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misclases;

import javax.swing.JOptionPane;

/**
 *
 * @author hp
 */
public class jintFactura2 extends javax.swing.JInternalFrame {

    /**
     * Creates new form jintFactura2
     */
    public jintFactura2() {
        initComponents();
        this.deshabilitar();
    }
    
    public void deshabilitar(){
        this.txtDescripcion.setEnabled(false);
        this.txtDomicilio.setEnabled(false);
        this.txtFecha.setEnabled(false);
        this.txtImpuestos.setEnabled(false);
        this.txtNomCliente.setEnabled(false);
        this.txtNumFactura.setEnabled(false);
        this.txtRfc.setEnabled(false);
        this.txtTotalPagar.setEnabled(false);
        this.txtVentas.setEnabled(false);
        
        //botones
        this.btnMostrar.setEnabled(false);
        this.btnGuardar.setEnabled(false);
    }
    
    public void habilitar(){
        this.txtDescripcion.setEnabled(!false);
        this.txtDomicilio.setEnabled(!false);
        this.txtFecha.setEnabled(!false);
        this.txtNomCliente.setEnabled(!false);
        this.txtNumFactura.setEnabled(!false);
        this.txtRfc.setEnabled(!false);
        this.txtVentas.setEnabled(!false);
        this.btnGuardar.setEnabled(true);
    }
    
    public void limpiar(){
        this.txtDescripcion.setText("");
        this.txtDomicilio.setText("");
        this.txtFecha.setText("");
        this.txtImpuestos.setText("");
        this.txtNomCliente.setText("");
        this.txtNumFactura.setText("");
        this.txtRfc.setText("");
        this.txtTotalPagar.setText("");
        this.txtVentas.setText("");
        
        this.txtNumFactura.requestFocus();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        txtVentas = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        txtNumFactura = new javax.swing.JTextField();
        txtRfc = new javax.swing.JTextField();
        txtNomCliente = new javax.swing.JTextField();
        txtDomicilio = new javax.swing.JTextField();
        txtDescripcion = new javax.swing.JTextField();
        txtFecha = new javax.swing.JTextField();
        btnNuevo = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        btnMostrar = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtImpuestos = new javax.swing.JTextField();
        txtTotalPagar = new javax.swing.JTextField();
        btnLimpiar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnSalir = new javax.swing.JButton();

        setBackground(new java.awt.Color(204, 0, 51));
        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Factura");
        getContentPane().setLayout(null);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(102, 255, 255));
        jLabel1.setText("Total de Ventas:");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(370, 90, 170, 20);

        txtVentas.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        getContentPane().add(txtVentas);
        txtVentas.setBounds(540, 90, 110, 20);

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(102, 255, 255));
        jLabel8.setText("Numero de Factura:");
        getContentPane().add(jLabel8);
        jLabel8.setBounds(20, 30, 170, 20);

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(102, 255, 255));
        jLabel9.setText("RFC:");
        getContentPane().add(jLabel9);
        jLabel9.setBounds(20, 60, 50, 20);

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(102, 255, 255));
        jLabel10.setText("Nombre del Cliente:");
        getContentPane().add(jLabel10);
        jLabel10.setBounds(20, 90, 170, 20);

        jLabel11.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(102, 255, 255));
        jLabel11.setText("Domicilio Fiscal:");
        getContentPane().add(jLabel11);
        jLabel11.setBounds(20, 120, 170, 20);

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(102, 255, 255));
        jLabel12.setText("Descripcion:");
        getContentPane().add(jLabel12);
        jLabel12.setBounds(370, 30, 170, 20);

        jLabel13.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(102, 255, 255));
        jLabel13.setText("Fecha:");
        getContentPane().add(jLabel13);
        jLabel13.setBounds(370, 60, 170, 20);

        txtNumFactura.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        getContentPane().add(txtNumFactura);
        txtNumFactura.setBounds(190, 30, 150, 20);

        txtRfc.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        getContentPane().add(txtRfc);
        txtRfc.setBounds(190, 60, 150, 20);

        txtNomCliente.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        getContentPane().add(txtNomCliente);
        txtNomCliente.setBounds(190, 90, 150, 20);

        txtDomicilio.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        getContentPane().add(txtDomicilio);
        txtDomicilio.setBounds(190, 120, 150, 20);

        txtDescripcion.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        getContentPane().add(txtDescripcion);
        txtDescripcion.setBounds(540, 30, 170, 20);

        txtFecha.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        getContentPane().add(txtFecha);
        txtFecha.setBounds(540, 60, 110, 20);

        btnNuevo.setText("Nuevo");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });
        getContentPane().add(btnNuevo);
        btnNuevo.setBounds(420, 150, 63, 23);

        btnGuardar.setText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });
        getContentPane().add(btnGuardar);
        btnGuardar.setBounds(530, 150, 71, 23);

        btnMostrar.setText("Mostrar");
        btnMostrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMostrarActionPerformed(evt);
            }
        });
        getContentPane().add(btnMostrar);
        btnMostrar.setBounds(640, 150, 69, 23);

        jPanel1.setBackground(new java.awt.Color(204, 204, 204));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "----Calculos de la Factura ----", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 18))); // NOI18N
        jPanel1.setLayout(null);

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("Total a Pagar:");
        jPanel1.add(jLabel2);
        jLabel2.setBounds(330, 90, 100, 17);

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setText("Impuestos:");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(80, 90, 100, 17);

        txtImpuestos.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jPanel1.add(txtImpuestos);
        txtImpuestos.setBounds(190, 90, 100, 23);

        txtTotalPagar.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jPanel1.add(txtTotalPagar);
        txtTotalPagar.setBounds(440, 90, 100, 23);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(80, 240, 630, 200);

        btnLimpiar.setText("Limpiar");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });
        getContentPane().add(btnLimpiar);
        btnLimpiar.setBounds(70, 480, 70, 23);

        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        getContentPane().add(btnCancelar);
        btnCancelar.setBounds(370, 480, 100, 23);

        btnSalir.setText("Salir");
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });
        getContentPane().add(btnSalir);
        btnSalir.setBounds(670, 480, 53, 23);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        // TODO add your handling code here:
        this.limpiar();
        this.deshabilitar();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnMostrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMostrarActionPerformed
        // TODO add your handling code here:
        
        this.txtNumFactura.setText(String.valueOf(con.getNumFactura()));
        this.txtDescripcion.setText(con.getDescripcion());
        this.txtRfc.setText(con.getRfc());
        this.txtDomicilio.setText(con.getDomicilioFiscal());
        this.txtNomCliente.setText(con.getNombreClinete());
        this.txtVentas.setText(String.valueOf(con.getTotalVenta()));
        this.txtFecha.setText(con.getFechaVenta());
        
        this.txtImpuestos.setText(String.valueOf(con.calcularImpuesto()));
        this.txtTotalPagar.setText(String.valueOf(con.calcularTotalPagar()));
    }//GEN-LAST:event_btnMostrarActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        // TODO add your handling code here:
        boolean exito = false;
            if(this.txtDescripcion.getText().equals("")) exito = true;
            if(this.txtDomicilio.getText().equals(""))exito = true;
            if(this.txtFecha.getText().equals(""))exito = true;
            if(this.txtNomCliente.getText().equals(""))exito = true;
            if(this.txtNumFactura.getText().equals(""))exito = true;
            if(this.txtRfc.getText().equals(""))exito = true;
            if(this.txtVentas.getText().equals(""))exito = true;
            
        if(exito == true){
            
            JOptionPane.showMessageDialog(this, "Falto capturar informacion!");
        }
        else{
            
            con.setDescripcion(this.txtDescripcion.getText());
            con.setDomicilioFiscal(this.txtDomicilio.getText());
            con.setFechaVenta(this.txtFecha.getText());
            con.setNombreClinete(this.txtNomCliente.getText());
            con.setNumFactura(Integer.parseInt(this.txtNumFactura.getText()));
            con.setRfc(this.txtRfc.getText());
            con.setTotalVenta(Float.parseFloat(this.txtVentas.getText()));
            
            JOptionPane.showMessageDialog(this, "Se aguardo con exito!");
            this.btnMostrar.setEnabled(true);
            
        }
        
        
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        // TODO add your handling code here:
        con = new Factura();
        this.habilitar();
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        // TODO add your handling code here:
        
        int opcion = 0;
        
        opcion = JOptionPane.showConfirmDialog(this, "Desea salir?", "Factura", JOptionPane.YES_NO_OPTION);
        if(opcion == JOptionPane.YES_OPTION){
            this.dispose();
        }
    }//GEN-LAST:event_btnSalirActionPerformed

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
        // TODO add your handling code here:
        this.limpiar();
    }//GEN-LAST:event_btnLimpiarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JButton btnMostrar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JButton btnSalir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField txtDescripcion;
    private javax.swing.JTextField txtDomicilio;
    private javax.swing.JTextField txtFecha;
    private javax.swing.JTextField txtImpuestos;
    private javax.swing.JTextField txtNomCliente;
    private javax.swing.JTextField txtNumFactura;
    private javax.swing.JTextField txtRfc;
    private javax.swing.JTextField txtTotalPagar;
    private javax.swing.JTextField txtVentas;
    // End of variables declaration//GEN-END:variables

    
    private Factura con;

}
